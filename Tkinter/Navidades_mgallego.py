import tkinter as tk
from tkinter import messagebox
from tkinter import *
from tkinter import Menu
from tkinter.ttk import Progressbar
from tkinter import ttk


# Principal
myWindow = tk.Tk()
myWindow.resizable(0,0) 
myWindow.title("Navidades 2020")
myWindow.geometry("400x600")
myWindow['bg'] = 'lightcoral'

# Menu de arriba

menu = Menu(myWindow)

new_item = Menu(menu)

# Quitar unas lineas raras

new_item = Menu(menu, tearoff=0)


# Funciones a las listas 

def clic():
    messagebox.showinfo('Bienvenid@', 'Bienvenido al proyecto de las Navidades 2020, espero que te guste!')

def click():
    messagebox.showinfo('¿Quienes Somos?', 'Soy Marc Gallego, el autor/creador de esta aplicación')

def clicke():
    messagebox.showinfo('¿Que es esto?', 'Esto es un proyecto de python que he hecho con tkinter. ¿Es cutre? La respuesta es SI pero estoy empezando y esto es lo que he podido hacer.(Cuando vaya aprendiendo la iré mejorando)')

def clicked():
    messagebox.showinfo('¿Cual es mi idea?', 'No hay una idea en concreto, esta aplicación la he creado para aprender algunas de las cosas básicas de tkinter en python como texto, dimensiones, funciones ...')

# Crear las barra de arriba 

menu.add_cascade(label='Archivos', menu=new_item)

# Crear las listas 

new_item.add_command(label='Bienvenid@', command=clic)

new_item.add_separator()   
 
new_item.add_command(label='¿Quienes Somos?', command=click)

new_item.add_separator()

new_item.add_command(label='¿Que es esto?', command=clicke)

new_item.add_separator()

new_item.add_command(label='¿Cual es mi idea?', command=clicked)

new_item.add_separator()



# Cerrar Programa

new_item.add_command(label='Cerrar', command = myWindow.destroy)

new_item.add_separator()

myWindow.config(menu=menu)

# ------------------------------------------------------------------------------------------------------------------------------------------

# Crear Texto
lbl = Label(myWindow, text="FELIZ NAVIDAD 2020", font=("Arial Bold", 30), fg="black")
lbl['bg'] = 'lightcoral'
lbl.place(x=0, y=0)

lbl = Label(myWindow, text="Covid-19 Edition", font=("Staatliches", 10), fg="black")
lbl['bg'] = 'lightcoral'
lbl.place(x=150, y=40)


# Crear Función
def vacia():
   messagebox.showinfo("¿App Vacía?", "Haz click arriba donde pone Archivos para descubrir más opciones ")

# Crear Botón
btn1 = tk.Button(myWindow, text = "¿App Vacía?", command = vacia, borderwidth=10)

btn1.place(x=150, y=75)




# Crear Texto
lbl = Label(myWindow, text="Progreso de la App", font=("Arial Bold", 20), fg="black")
lbl['bg'] = 'lightcoral'
lbl.place(x=90, y=500)



# Crear barra de progreso
style = ttk.Style()

style.theme_use('default')

style.configure("black.Horizontal.TProgressbar", background='green')

bar = Progressbar(myWindow, length=200, style='black.Horizontal.TProgressbar')

bar['value'] = 20

bar.place(x=100, y=550)


myWindow.mainloop()
