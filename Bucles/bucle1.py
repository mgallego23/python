# coding: utf-8
import time

print("")
print("")
print("¡¡ PRIMER BUCLE !!")
print("")
print("Aprende del 1 al 50 ")
print("")




# Inicializaciones
salir = "N"
num = 1 

while ( salir=="N" ):
    # Hago cosas
    print("Este es el numero", num)

    time.sleep(0.5)

    # Incremento
    num = num + 1
    # Activo indicador de salida si toca
    if ( num > 50 ): # Condición de salida
        salir = "S"