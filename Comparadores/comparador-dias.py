# coding: utf-8
	
print("")
print("")
print("¡¡ COMPARADOR DE DIAS !!")
print("")
print("Cuantos dias quedan/faltan para llegar a ese año ")
print("")

diaactual=int(input("¿En que año estamos?="))
diacualquiera=int(input("Escriba un año cualquiera="))

resultado= (diaactual - diacualquiera)*365
resultadopositivo= (diacualquiera - diaactual)*365

if(diaactual==diacualquiera):
    print("¡Son el mismo año!")

else:
    if(diaactual<diacualquiera):
        print("Para llegar al año", diacualquiera, "faltan", resultadopositivo, "días.")
    else:
        if(diaactual>diacualquiera):
            print("Desde el año", diacualquiera, "han pasado", resultado,  "días.")