# coding: utf-8
	
print("")
print("")
print("¡¡ COMPARADOR DE MESES !!")
print("")
print("Cuantos meses quedan/faltan para llegar a ese año ")
print("")

mesactual=int(input("¿En que año estamos?="))
mescualquiera=int(input("Escriba un año cualquiera="))

resultado= (mesactual - mescualquiera)*12
resultadopositivo= (mescualquiera - mesactual)*12

if(mesactual==mescualquiera):
    print("¡Son el mismo año!")

else:
    if(mesactual<mescualquiera):
        print("Para llegar al año", mescualquiera, "faltan", resultadopositivo, "meses.")
    else:
        if(mesactual>mescualquiera):
            print("Desde el año", mescualquiera, "han pasado", resultado,  "meses.")