## **SOBRE MI:**

Me relaciono muy bien cara al público ya que tengo experiencia como ayudante de monitor. Me encantaría formar parte de una empresa en la que poder aplicar todos mis conocimientos y, al mismo tiempo, desarrollarme y especializarme tanto personalmente como profesionalmente. Considero que puedo aportar valores, habilidades y conocimientos.

**FECHA DE NACIMIENTO**  19/12/2003

**NACIONALIDAD:** Española

**SEXO:** Masculino


## *CONTACTO:*

✉  marcgallego2003@gmail.com

Ins Camí de Mar, Calafell (43820)



## Experiencia Laboral:



***Ayudante Monitor***

Trabajé 2 años seguidos en verano de ayudante de monitor en Liberty Sports.

Verano agosto 2018

Verano agosto 2019

Liberty Sports - Calafell, Tarragona provincia



## Idiomas:



**CASTELLANO:** Lengua Materna

**CATALÁN:** Nivel Alto (Hablado y escrito)

**INGLÉS:** Nivel Medio (Hablado y escrito)


## ESTUDIOS:

***ESO:***

Escuela: Camp Joliu

Septiembre 2015 - Junio 2019

***FP Grado Medio de Sistemas Microinformáticos y Redes***

Escuela: Camí de Mar

Septiembre 2019 - (Actualmente en curso)